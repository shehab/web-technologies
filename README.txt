# This README explains basic aspects of the VM provided and the tool functioning

The virtual machine provided is a Lubuntu 14.04 prepared in order to use the following tools to develop the practice:
	- Eclipse EE in order to code and deploy the web application
	- GlassFish 4.0 in order to run the application into a server (similar to tomcat, but is the one required)

In order to start working, first things first:
	- The terminal tool for Lubuntu is XTerm (under system tools > Xterm)
	- It is necessary to download the GIT repository from https://<<youruser>>@bitbucket.org/100282685/web-technologies.git
	- In order to test the functionalities of the application, just follow the first lines described in the notes of the first class (in this same folder of the repo)


 ------------THE USER AND PASSWORD FOR THE VM ARE ---------------

	user: tiwuc3m
	pass: tiwuc3m
