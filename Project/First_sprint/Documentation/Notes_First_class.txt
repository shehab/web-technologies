	omic web project
2. Continue, continue, the final one check “Generate web.xml deployment descriptor”

General notes

We will use .jsp files. They are composed of HTML, and each time you want to use a java line you have to insert it between the tags “<% whatever %>”

A servlet is a class which extends “httpservlet”, and which is going to be in charge of processing data in the servlet. A servlet has a unique name within the application, which is specified on top of the declaration with the tag “ @webservlet ”

To communicate between a client and a servlet, we can use either get or post. While get passes the parameters via URL (all parameters values and names go in the url, which is less secure), POST requests send the parameters hidden and not in the URL.

A POST and GET method must be implemented in both files. Their parameters comprise HTTPServletRequest and HTTPServletResponse. Those objects contains methods to interact with them (i.e. instead of System.Out.Println we will use response.getWriter() in order to write to the standard output of the response).
